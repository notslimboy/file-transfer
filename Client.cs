using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Text;

namespace FTPClient
{
    class client
    {
        static void Main(string[] args)
        {
            Console.WriteLine(" [Waiting...]");
            TcpClient tcpClient = new TcpClient("192.168.43.134", 5656);
            StreamWriter writer = new StreamWriter(tcpClient.GetStream());

            try
            {
                Console.Write(" [Please enter a full file path] ");
                string directory = Console.ReadLine();

                Console.WriteLine(" [Waiting. Sending file.]");

                byte[] bytes = File.ReadAllBytes(directory);

                writer.WriteLine(bytes.Length.ToString());
                writer.Flush();

                writer.WriteLine(directory);
                writer.Flush();

                Console.WriteLine(" [File Sent]");
                tcpClient.Client.SendFile(directory);
            }
            catch (Exception e)
            {
                Console.Write(e.Message);
            }

            Console.Read();
        }
    }
}